#!/bin/bash -v

# Install Java
# yum -y install java-1.8.0-openjdk-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-headless-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-devel-1.8.0.151-1.b12.35.amzn1.x86_64
# yum -y erase java-1.7.0-openjdk
#
# # Download the petclinic app from s3
# [[ ! -d /opt/petclinic ]] && mkdir /opt/petclinic
# aws s3 cp s3://${bucket}/spring-petclinic-2.0.0.jar /opt/petclinic
#
# # Copy the start/stop script
# aws s3 cp s3://${bucket}/petclinic /etc/init.d/petclinic
# chmod +x /etc/init.d/petclinicp
#
# # Set the application properties
# cat >/opt/petclinic/application.properties <<_END_
# # database init, supports mysql too
# database=mysql
# spring.datasource.url=jdbc:mysql://${dbsrv}/${dbname}
# spring.datasource.username=${dbuser}
# spring.datasource.password=${dbpw}
#
# # Web
# spring.thymeleaf.mode=HTML
#
# # JPA
# spring.jpa.hibernate.ddl-auto=none
#
# # Internationalization
# spring.messages.basename=messages/messages
#
# # Actuator / Management
# management.endpoints.web.base-path=/manage
# # Spring Boot 1.5 makes actuator secure by default
# management.endpoints.web.enabled=true
#
# # Logging
# logging.level.org.springframework=INFO
#  logging.level.org.springframework.web=DEBUG
#  logging.level.org.springframework.context.annotation=TRACE
#
# # Active Spring profiles
# spring.profiles.active=production
# _END_
#
#chkconfig --add petclinic
#sudo service petclinic start
