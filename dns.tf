
/* Add this zone to the main one */
resource "aws_route53_record" "danishtf" {
  zone_id = "Z2U00Q95U7EKEA"
  name = "${var.dns_domain}"
  type = "A"
  ttl = "300"

  records = [
    "${aws_instance.webinstance.*.public_ip}"
  ]
}
