variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {
	description = "AWS Keyname for SSH"
	default = "davekey"
}

variable "dns_domain" {
	description = "DNS top level name for your app"
	default = "danishtf.grads.al-labs.co.uk"
}

variable "db_name" {
	description = "DB name for app"
	default = "petclinic"
}

variable "db_user" {
	description = "DB username"
	default = "petclinic"
}

variable "db_password" {
	description = "DB user password"
	default = "petclinic"
}

variable "my_tag" {
	description = "Name to tag all elements with"
	default = "danish"
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        eu-west-1 = "ami-d834aba1"
				eu-west-2 = "ami-403e2524"
				us-east-1 = "ami-97785bed"
				eu-central-1 = "ami-5652ce39"
    }
}

variable "pcami" {
    description = "AMI for PC"
    default = "ami-78a36305"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.1.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.1.0.0/24"
}

variable "public_subnet_cidr2" {
    description = "CIDR for the Public Subnet"
    default = "10.1.2.0/24"
}

variable "sshlocation" {
    description = "CIDR of location allowed SSH"
    default = "77.108.144.180/32"
}

variable "weblocation" {
    description = "CIDR of location allowed WEB access"
    default = "0.0.0.0/0"
}

variable "endpoint" {
    description = "Endpoint of the RDS"
    default = "vazabase2.cmsrwfklauzv.us-east-1.rds.amazonaws.com"
}
