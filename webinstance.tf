/*
  Web Server Instances
*/

resource "aws_security_group" "webinstance" {
    name = "vpc_webinstance"
    description = "Allow incoming from VPC subnets only"

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["${var.weblocation}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.sshlocation}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.my_tag}_webInstanceSG"
      }
}

data "template_file" "webinstance" {
  template = "${file("templates/web.tpl")}"

  vars {
    dbsrv = "${var.endpoint}"
    dbuser = "${var.db_user}"
    dbpw = "${var.db_password}"
    dbname = "${var.db_name}"
    bucket = "${lower(var.my_tag)}-tfstore"
  }
}

resource "aws_instance" "webinstance" {
    /*depends_on = ["aws_s3_bucket_object.petclinic","aws_s3_bucket_object.springpet"]
    */
    count=2
    ami = "${var.pcami}"
    availability_zone = "${var.aws_region}a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.webinstance.id}"]
    subnet_id = "${aws_subnet.public.id}"
    associate_public_ip_address = true
    source_dest_check = false
    iam_instance_profile = "s3Vault"

    root_block_device {
        volume_size = 8
    }

    user_data = "${file("templates/web.tpl")}"

    tags {
        Name = "${var.my_tag}_Web_${count.index}"
    }
}
