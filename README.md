## Terraform task: 

Build the above application infrastructure.  You should use your existing PetClinic Jar, and there is no need to worry about the rebuild of the app in this case, but the Petclinic JAR should be in an S3 bucket that you will all pull from.

The database will be created and the schema added on initial run of Terraform.

The 2 instances live in different AZs, so you require 2 public subnets.  The MySQL database must only be accessible by the 2 public subnets.

You will use Route 53 DNS to load balance your application, so you will have 1 DNS record with 2 public IPs.



Allowed changes;

Use existing RDS (must be public to allow all VPCs to use it)
1 public subnet, but you will need 2x EC2 instances
Existing S3 bucket containing the Petclinic JAR that works with MySQL DB.


Some help.

Take a look at this bigger version of the project, you may be able to use some of the code:
https://JangleFett@bitbucket.org/JangleFett/terraformlab.git